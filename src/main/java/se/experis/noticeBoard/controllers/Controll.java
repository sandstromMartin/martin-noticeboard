package se.experis.noticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.noticeBoard.Models.Notice;
import se.experis.noticeBoard.Models.Reply;
import se.experis.noticeBoard.Repositories.NoticeRepository;
import se.experis.noticeBoard.Repositories.ReplyRepository;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class Controll {

    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private ReplyRepository replyRepository;

    @GetMapping("/")
    public String getEntry(HttpServletRequest request, Model model) {
        List<Notice> notices = noticeRepository.findAllByOrderByPublishedOnDesc();
        model.addAttribute("allnotice", notices);
        return "all";
    }

    @GetMapping("/add-notice")
    public String addEntry(Model model) {
        return "addnotice";
    }

    @GetMapping("/add-reply/{id}")
    public String addReply (@PathVariable int id, Model model){
        Notice notice = noticeRepository.getById(id);
        model.addAttribute("notices", notice);
        return "addreply";
    }

    @GetMapping("/edit-notice/{id}")
    public String editController(@PathVariable("id") String id, Model model) {
//        Reply reply=replyRepository.findById(Integer.parseInt(id)).get();
//        model.addAttribute("theReply",reply);

        Notice notice= noticeRepository.findById(Integer.parseInt(id)).get();
        model.addAttribute("theNotice", notice);
        return "editnotice";
    }

}