package se.experis.noticeBoard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeBoard.Models.CommonResponse;
import se.experis.noticeBoard.Models.Notice;
import se.experis.noticeBoard.Models.Reply;
import se.experis.noticeBoard.Repositories.NoticeRepository;
import se.experis.noticeBoard.Repositories.ReplyRepository;
import se.experis.noticeBoard.Utils.Command;
import se.experis.noticeBoard.Utils.Logger;
import se.experis.noticeBoard.Utils.SessionKeeper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;


@RestController
public class RestControll {

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private ReplyRepository replyRepository;


    @GetMapping("/notice/{id}")
    public ResponseEntity<CommonResponse> getEntryById(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if (noticeRepository.existsById(id)) {
            cr.data = noticeRepository.findById(id);
            cr.message = "Entry with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Entry not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }
    @GetMapping("/reply/{id}")
    public ResponseEntity<CommonResponse> getReplyById(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if (replyRepository.existsById(id)) {
            cr.data = replyRepository.findById(id);
            cr.message = "Reply with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Entry not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/add-notice")
    public ResponseEntity<Notice> addEntry(HttpServletRequest request, @RequestBody Notice notice, HttpSession session) {
        String message = "";
        HttpStatus resp;

        if (SessionKeeper.getInstance().CheckSession(session.getId())) {
            message = "ADDED!";
            resp = HttpStatus.CREATED;
            notice = noticeRepository.save(notice);
            System.out.println("New entry with id: " + notice.id);
        } else {
            message = "FAIL";
            resp = HttpStatus.OK;
        }


        return new ResponseEntity<>(notice, resp);
    }

/*
    @PostMapping("/add-reply")
    public ResponseEntity<Reply> addReply(HttpServletRequest request, @RequestBody Reply reply) {
        reply = replyRepository.save(reply);
        System.out.println("New entry with id: " + reply.id);
        HttpStatus resp = HttpStatus.CREATED;
        return new ResponseEntity<>(reply, resp);
    }
*/

   @PostMapping("/add-reply/{noticeId}")
    public ResponseEntity<CommonResponse> createNewReplyIntoNotice(@RequestBody Reply reply, @PathVariable Integer noticeId) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(noticeRepository.existsById(noticeId)) {
            Optional<Notice> optionalNotice = noticeRepository.findById(noticeId);
            Notice notice = optionalNotice.get();
            notice.replies.add(reply);
            notice = noticeRepository.save(notice);
            cr.data = notice;
            cr.message = "New reply created!";
            resp = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "No notice post has id "+ noticeId;
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEntry(HttpServletRequest request, @PathVariable Integer id, HttpSession session) {
        String message = "";
        HttpStatus resp;

        if (noticeRepository.existsById(id) && SessionKeeper.getInstance().CheckSession(session.getId())) {
            noticeRepository.deleteById(id);
            System.out.println("Deleted entry with id: " + id);
            message = "GONE!";
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found with id: " + id);
            message = "FAIL";
            resp = HttpStatus.OK;
        }
        return new ResponseEntity<>(message, resp);
    }

    @PatchMapping("/edit/{id}")
    public ResponseEntity<Notice> editEntry(HttpServletRequest request, @RequestBody Notice newNotice, @PathVariable Integer id) {
        Notice notice = null;
        HttpStatus resp = null;

        if (noticeRepository.existsById(id)) {
            Optional<Notice> entryRepo = noticeRepository.findById(id);
            notice = entryRepo.get();
        }

        if (newNotice.noticeTopic != null) {
            notice.noticeTopic = newNotice.noticeTopic;
        }
        if (newNotice.noticeText != null) {
            notice.noticeText = newNotice.noticeText;
        }
        /*if (newNotice.imageURL != null) {
            notice.imageURL = newNotice.imageURL;
        }*/


        noticeRepository.save(notice);
        System.out.println("Updated entry with id: " + notice.id);
        resp = HttpStatus.OK;
        return new ResponseEntity<>(notice, resp);
    }
}

