package se.experis.noticeBoard.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
@Entity
//@Table(name = "Notice")
    public class Notice {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        public Integer id;
        @Column
        public String noticeAuthor;
        @Column
        public String noticeTopic;
        @Column
        public String noticeText;

  /*  @JsonGetter("replies")
    public List<String> replies() {
        return replies.stream()
                .map(reply -> {
                    return reply.id + reply.replyAuthor + reply.replyText+reply.publishedOn;
                }).collect(Collectors.toList());
    }*/

    @OneToMany(targetEntity=Reply.class,cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true )

    public Set<Reply> replies = new HashSet<>();

        @Column
        @CreationTimestamp
        @Temporal(TemporalType.TIMESTAMP)
        public java.util.Date publishedOn;


}
