package se.experis.noticeBoard.Models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
@Entity
public class Reply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column
    public String replyText;
    @Column
    public String replyAuthor;
    @Column
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    public java.util.Date publishedOn;

    @ManyToOne(fetch = FetchType.LAZY)
    private Notice notice;

}
