package se.experis.noticeBoard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeBoard.Models.Reply;

import java.util.List;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Reply getById(int id);
    List<Reply> findAllByOrderByPublishedOnDesc();

}
