package se.experis.noticeBoard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeBoard.Models.Notice;

import java.util.List;

public interface NoticeRepository extends JpaRepository<Notice, Integer> {
    Notice getById(int id);
    List<Notice> findAllByOrderByPublishedOnDesc();
}


